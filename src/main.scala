
object main {
  import scala.collection.mutable.ArrayBuffer

  val t0 = System.nanoTime()
  def isPalindrome(s: String): Boolean = (s.size >= 2) && s == s.reverse

  var Palindroms = ArrayBuffer[Int]()

  for (x <- 100 until 1000) {

    for (y <- x until 1000) {
      var prod = (x * y).toString
      if (isPalindrome(prod)) {
        Palindroms += (prod.toInt)
      }

    }
  }
  println(Palindroms.sorted.reverse(0))
  val t1 = System.nanoTime()
  println("Elapsed time: " + (t1 - t0)*0.000000001 + "s")
}